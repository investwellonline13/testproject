package Broker;

import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Action;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Functions.Global_Methods;
import Generic.BaseLib;
import Generic.DatabaseConfig;
import Generic.WaitStatement;
import Page_Elements.Broker_Folio_Lookup_Page_Elements;

public class Broker_Folio_Lookup extends BaseLib{

		
	@Test(priority = 1, description = "This test case verifies Folio Lookup URL")
	public void Verify_FolioLookup_URL() throws SQLException, Exception {
		Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
		Global_Methods gm =  new Global_Methods(driver);
		DatabaseConfig db= new DatabaseConfig();

		System.out.println("Test Case 1 Executed");
		WaitStatement.iSleep(2);

		fl.click_FolioLookup();
		WaitStatement.iSleep(2);

		String actual = driver.getCurrentUrl();
//		String actual=fl.getcurrentURL();
		WaitStatement.iSleep(2);

		String expected = "http://demo.investwellonline.com/app/#/broker/folioLookup";
		SoftAssert sa = new SoftAssert();
		sa.assertEquals(actual, expected);
		WaitStatement.iSleep(2);

		sa.assertAll();
		String clientname=db.Database_SelectQuery_Execution("select name from clients where appid='2'","name");
		System.out.println("check for get client name:" +clientname);
		
	}

	@Test(priority = 2, enabled = false, description = "This test case verifies the Header text")
	public void Verify_Header_Text(){
		Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

		System.out.println("Test Case 2 Executed");

		WaitStatement.iSleep(3);
		fl.Header_text_verify();
	}

	@Test(priority = 4, enabled = false, description = "This test case verifies Table Header Row")
	public void Verify_Table_Header_Row() {
		Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

		System.out.println("Test Case 4 Executed");
		fl.Verify_Table_Header();
	}

	@Test(priority = 5, enabled = false, description = "This test case verifies Searching Functionality with Folio")
	public void Verify_SearchFolio_Functionality() {
		Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
		Global_Methods gm =  new Global_Methods(driver);

		System.out.println("Test Case 5 Executed");
		gm.Refresh_Page();
		// driver.navigate().refresh();
		WaitStatement.iSleep(2);
		fl.Verify_SearchFolio_Functionality();
	}
	
	
	 @Test(priority=7, enabled=false, description="This test case verifies Searching Functionality with Investor Name")
	 public void Verify_SearchInvestor_Functionality() 
	 {
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm =  new Global_Methods(driver);

	 System.out.println("Test Case 7 Executed");
	 gm.Refresh_Page();
WaitStatement.iSleep(2);
	 fl.Verify_SearchInvestor_Functionality();
	 }
	 
	 
	 @Test(priority=8, enabled=false, description="This test case verifies Searching Functionality with Scheme Name")
	 public void Verify_SearchScheme_Functionality()
	 {
	Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
	Global_Methods gm =  new Global_Methods(driver);

	 System.out.println("Test Case 8 Executed");
	 gm.Refresh_Page();
	 WaitStatement.iSleep(2);
	 fl.ApplyFilter();
	 fl.Verify_SearchScheme_Functionality();
	 }
	
	
	
	 @Test(priority=9,enabled=false, description="This test case verifies searching functionality with applicant mapped name")
	 public void Verify_SearchApplicant_Mapped() 
	 {
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm =  new Global_Methods(driver);

	 System.out.println("Test case 9 Executed");
//	 gm.Refresh_Page();
	 WaitStatement.iSleep(2);
	 fl.Verify_SearchApplicant_Mapped();
	 }
	
	 @Test(priority=10, enabled=false, description="This test case verifies PAN search functionality")
	 public void Verify_SearchPAN() 
	 {
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

	 System.out.println("Test case 10 Executed");
	 WaitStatement.iSleep(2);
	 fl.Verify_SearchPAN();
	 }
	
	 @Test(priority=11,enabled=false, description="This test case Verifies IIN UCC CAN seacrh functionality")
	 public void Verify_IINCANUCCSearch() 
	 {
	  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

	 System.out.println("Test case 11 Executed");
	 WaitStatement.iSleep(2);
	 fl.Verify_IINCANUCCSearch();
	
	 }
	 
	 @Test(priority=12,enabled=false, description="This test case verifies Tex Status search functionality")
		public void Verify_TexStatus()
		{
		  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
		  WaitStatement.iSleep(2);
			System.out.println("Test case 12 Executed");
			fl.Verify_TexStatusSearch();
		}
		
		@Test(priority=13,enabled=false, description="This test case verifies Mode of holding search functionality")
		public void Verify_ModeOfHolding()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 13 Executed");
			fl.Verify_ModeOfHolding();
		}
		@Test(priority=14,enabled=false, description="This test case verifies JOint 1 Name search functionality")
		public void Verify_JOint1Name()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 14 Executed");
			fl.Verify_Joint1Name();
		}
		@Test(priority=15,enabled=false, description="This test case verifies JOint2 PAN search functionality")
		public void Verify_Joint1PAN()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 15 Executed");
			fl.Verify_Joint1PAN();
		}
		@Test(priority=16,enabled=false, description="This test case verifies Joint2 Name search functionality")
		public void Verify_Joint2Name()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 16 Executed");
			fl.Verify_Joint2Name();
		}
		@Test(priority=17,enabled=false, description="This test case verifies Joint2 PAN search functionality")
		public void Verify_Joint2PAN()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 17 Executed");
			fl.Verify_Joint2PAN();
		}
		@Test(priority=18,enabled=false, description="This test case verifies Guardian Name search functionality")
		public void Verify_GuardianName()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 18 Executed");
			fl.Verify_GuardianName();
		}
		@Test(priority=19,enabled=false, description="This test case verifies Guardian PAN search functionality")
		public void Verify_GuardianPAN()
		{
			  Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				WaitStatement.iSleep(2);

			System.out.println("Test case 19 Executed");
			fl.Verify_GuardianPAN();
		}
	
		@Test(priority=20,enabled=false, description="This case verifies Brief toggle all search functionality")
		public void Verify_Brief()
		{
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm=new Global_Methods(driver);
		    WaitStatement.iSleep(2);
		    System.out.println("Test case 20 Executed");
		    gm.Refresh_Page();
		    fl.Verify_Brief();
		    fl.Verify_SearchScheme_Functionality();
		    fl.Verify_SearchApplicant_Mapped();
		    fl.Verify_SearchPAN();
		    fl.Verify_IINCANUCCSearch();

			
		}
		
		@Test(priority=21, enabled=false, description="This Test Case Verifies Amount Row should have comma separated values")
		public void Verify_CommaSeperatedValues() 
		{
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm=new Global_Methods(driver);
			System.out.println("Test Case 21 Executed");
			gm.Refresh_Page();
			WaitStatement.iSleep(2);
			fl.ApplyFilter();
			WaitStatement.iSleep(2);
			fl.CommaSeperatedValues();		
			
		}
		
		@Test(priority=22,enabled=false, description="This Test Case Verifies Amount Row is Right ALigned")
		public void Verify_AmountRow_Right_Aligned()
		{
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm=new Global_Methods(driver);
			System.out.println("Test Case 22 Executed");
//			gm.Refresh_Page();
//			WaitStatement.iSleep(2);
//			fl.ApplyFilter();
			WaitStatement.iSleep(2);
			fl.Verify_AmountRow_Right_Aligned();
		} 
		
		@Test(priority=23, enabled=false, description="This Test Case Verifies Folio Details overlay")
		public void Verify_Folio_Details()
		{
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm=new Global_Methods(driver);
			System.out.println("Test Case 23 Executed");
			gm.Refresh_Page();
			fl.ApplyFilter();
			WaitStatement.iSleep(2);
			gm.scroll_page_verticall(50);
			WaitStatement.iSleep(5);

			fl.Verify_FolioDetails();
		}
		
		
		@Test(priority=24,enabled=false,description="This test case verifies download txn slip of folio details")
		public void Verify_Download_Txnslip_FolioDetails() 
		{
			Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
			Global_Methods gm=new Global_Methods(driver);
			System.out.println("Test Case 24 Executed");
			gm.Refresh_Page();
			fl.ApplyFilter();
			gm.scroll_page_verticall(50);
			WaitStatement.iSleep(5);
			fl.Verify_Download_Txnslip_FolioDetails();
		}
		
		 @Test(priority=25,enabled=false,description="This test case verifies download txn of SOA")
		   public void Verify_Download_Txnslip_SOA() throws Exception
		   {
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
			   System.out.println("Test Case 25 Executed");
				gm.Refresh_Page();
				fl.ApplyFilter();
				gm.scroll_page_verticall(50);
				WaitStatement.iSleep(5);
				fl.Verify_Download_Txnslip_SOA();
			   
		   }
		 
		 @Test(priority=26, enabled=false, description="This Test Case Verifies SOA Values2")
			public void Verify_SOA_Values2() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test Case 26 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(2);

				fl.ApplyFilter();
				WaitStatement.iSleep(5);
				fl.Verify_SOA_values2();
			}
		 
		 @Test(priority=27, enabled=false, description="This Test Case Verifies decimal points should be 2 for Amount Row under SOA")
			public void Verify_SOA_2Decimal_For_Amount_Row() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				System.out.println("Test Case 27 Executed");
				WaitStatement.iSleep(2);
				fl.Verify_SOA_2Decimal_For_Amount_Row();
			}
		 
		 @Test(priority=28, enabled=false, description="This Test Case Verifies decimal points should be 4 for NAV, Unit and Balance Units Rows")
			public void Verify_SOA_4Decimal_For_NAV_Unit_Balance_Rows() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

				System.out.println("Test Case 28 Executed");
				WaitStatement.iSleep(2);
				fl.Verify_SOA_4Decimal_For_NAV_Unit_Balance_Rows();
			}
		 
		 @Test(priority=29, enabled=false, description="This Test Case Verifies Amount Row should have comma separated values")
			public void Verify_SOA_AmountRow_Right_CommaSeparated() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);

				System.out.println("Test Case 29 Executed");
				WaitStatement.iSleep(2);
				fl.Verify_SOA_AmountRow_CommaSeparated();
			}
		 
	//action functionality===============================================================================================
		 
		 @Test(priority=30,enabled=false, description="This Test Case Verifies Actions on Folio Lookup")
			public void Verify_Folio_Lookup_Actions() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test Case 30 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(2);
				fl.ApplyFilter();
				WaitStatement.iSleep(2);
				fl.Verify_Folio_Lookup_Actions();
			}
		 
		 @Test(priority=31,enabled=false, description="This Test Case Verifies All the Features before and after performing Asign Folios to Existing Investor Action on Folio Lookup")
			public void Verify_AssignFolioToExistingInvestor_Functionality() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);

				System.out.println("Test Case 31 Executed");
				WaitStatement.iSleep(2);
				gm.Refresh_Page();
				WaitStatement.iSleep(2);
				fl.ApplyFilter();
				WaitStatement.iSleep(2);
				fl.Verify_AssignFolioToExistingInvestor_Functionality();
			}
		 
		 
		 @Test(priority=32,enabled=false, description="This Test Case Verifies the Assign folio to new Investor Functionality")
			public void Verify_AssignfoliotonewInvestor_Functionality() throws Exception
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test Case 32 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(2);
				fl.ApplyFilter();
				WaitStatement.iSleep(2);
				fl.Verify_AssignfoliotonewInvestor_Functionality();
			}
		 
		 
		 @Test(priority=33,enabled=false, description="This Test Case Verifies the Assign folio to new Investor Functionality")
			public void Verify_AssignfoliotonewInvestor_WithoutPAN_Functionality() 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test Case 33 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(3);
				fl.ApplyFilter();
				WaitStatement.iSleep(3);
				fl.Verify_AssignfoliotonewInvestor_WithoutPAN_Functionality();
				
			}
		 
		 @Test(priority=34,enabled=false, description="This test case verifies display mapped ucc and iin no")
			public void Verify_mapped_ucc_iin_number() throws Exception 
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test case 34 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(3);
				fl.ApplyFilter();
				WaitStatement.iSleep(3);
				fl.Verify_mapped_number();
				
			}
		 
		 @Test(priority=35,enabled=false, description="This test case verifies update IIN & update UCC")
			public void Verify_Update_UCC_Number() throws Exception
			{
			 Broker_Folio_Lookup_Page_Elements fl = new Broker_Folio_Lookup_Page_Elements(driver);
				Global_Methods gm=new Global_Methods(driver);
				System.out.println("Test case 31 Executed");
				gm.Refresh_Page();
				WaitStatement.iSleep(3);
				fl.ApplyFilter();
				WaitStatement.iSleep(3);
				gm.scroll_page_verticall(100);
				WaitStatement.iSleep(3);
				fl.Verify_Update_Number();
				
			}
		 
		 

}
