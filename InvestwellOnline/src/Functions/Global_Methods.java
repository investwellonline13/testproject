package Functions;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import Generic.BaseLib;
import Generic.WaitStatement;

public class Global_Methods extends BaseLib
{
//	private WebDriver driver;
	public Global_Methods(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
	
	public void sendkey(WebElement element, String text) {
		element.sendKeys(text);
	}
	public void click(WebElement element) {
		element.click();
	}
	
	public void Refresh_Page()
	{
		WaitStatement.iSleep(2);
	driver.navigate().refresh();
	WaitStatement.iSleep(2);
//	Database_Query_Execution(TabelName, column, wherecolum, wherevalue);
	}

	public void screen_resolution()
	{
		JavascriptExecutor js= (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom ='70%';");
		
		
	}
	
	public void Horizontal_scroll(WebElement targetElement, WebElement destinationTargetElement)
	{
		Actions act= new Actions(driver);
		act.clickAndHold(targetElement).build().perform();
		act.moveToElement(destinationTargetElement).build().perform();
		act.release();
	}
	
	public int scroll_page_verticall(int vertical_value)
	{
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0, "+vertical_value+")");
		return vertical_value;
	}
	
	@FindBy(xpath="//span[@class='visibleRows']")private WebElement last_Number_Pagination;

	public int Fetch_Last_Number_Pagination() 
	{
		String value=last_Number_Pagination.getText();
		String v[]=value.split(" ");
		v[4]=v[4].replace(".", "");
		int a=Integer.parseInt(v[4]);
	return a;
				
	}
	
	@FindBy(xpath="//div[@class='shortContents fl']")private List<WebElement> SelectionLine;
	
	public int fetch_number_from_selection_line()
	{
		int number = 0;
		 
		for(WebElement e: SelectionLine)
		{
			String str = e.getText();
			String []p=str.split(" ");
			number=Integer.parseInt(p[2]);
		}
		return number;
	}
	
	String downloadPath = "/home/swati/Downloads";
	public File getLatestFilefromDir(String downloadPath) 
	{
		
		File dir = new File(downloadPath);
	    File[] files = dir.listFiles();
//	    if (files == null || files.length == 0) {
//	        return null;
//	    }

	    File lastModifiedFile = files[0];
	    for (int i = 1; i < files.length; i++) {
	       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	           lastModifiedFile = files[i];
	       }
	    }
		return lastModifiedFile;
	}
	
	
	
    
}
