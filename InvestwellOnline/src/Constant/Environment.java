/**
 * 
 */
package Constant;

import org.aeonbits.owner.Config;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;



/**
 * @author swati
 *
 */

	@PropertySource(value={"/home/swati/eclipse-workspace/InvestwellOnline/config:${env}.properties"})
	public interface Environment extends Config {

	    String url();

	    String username();

	    String password();

	    @Key("db.hosturl")
	    String getDBHostname();


	    @Key("db.username")
	    String getDBUsername();

	    @Key("db.password")
	    String getDBPassword();

	}


