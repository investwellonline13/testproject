package Generic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitStatement extends BaseLib
{

	public WaitStatement(WebDriver driver)//constructor
	{
		this.driver=driver;
		
	}
	
	/*************hardcore method
	 * @return *************/
	public static void iSleep(int secs)//hardcore method
	{
		try{
		Thread.sleep(secs*1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
	}
	/*************IMPLICIT Wait*************/
	public void iWaitForSecs(int secs)//IMPLICIT Wait
	{
		driver.manage().timeouts().implicitlyWait(secs, TimeUnit.SECONDS);
	}
	
	/*************Explicit Wait*************/
	public void eWaitForVisible(int secs, WebElement ele)
	{
		WebDriverWait wait=new WebDriverWait(driver,secs);
		wait.until(ExpectedConditions.visibilityOf(ele));
	}
	/*************Refresh*************/
	public void eRefresh(int secs, WebElement ele)
	{
		WebDriverWait wait=new WebDriverWait(driver,secs);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(ele)));
	}


}
