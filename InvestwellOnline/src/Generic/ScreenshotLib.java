package Generic;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ScreenshotLib 
{
	
	public static void takescreenshot(WebDriver driver, String ScriptName)
	{
		
		String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());

		EventFiringWebDriver efw = new EventFiringWebDriver(driver);
		File srcFile = efw.getScreenshotAs(OutputType.FILE);
		File destFile = new File("/home/swati/eclipse-workspace/InvestwellOnline/Screenshot/"+ScriptName+ timestamp+".png");
		try
		{
			FileUtils.copyFile(srcFile, destFile);
		
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

	}
	
}
