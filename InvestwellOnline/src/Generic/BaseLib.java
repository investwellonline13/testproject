package Generic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import Constant.Environment;
import Page_Elements.Login_Page_Elements;

public class BaseLib {

	//PATH======================================================================================================
	
		String chrome_driver_path = "/usr/bin/chromedriver";
//		String Website_URL = "http://demo.investwellonline.com/app/#/login";
		String chrome_driver = "webdriver.chrome.driver";
		
		//INITIALIZATION============================================================================================
		
		public WebDriver driver;
	    Environment testEnvironment;
		
	    @Parameters({"environment"})

		@BeforeTest(alwaysRun = true)

		public void before_test_setup(String environment) throws IOException, ConfigurationException
		{	
			
	    	System.out.println("driver: "+driver);
			System.setProperty(chrome_driver, chrome_driver_path);
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			options.addArguments("start-maximized"); // open Browser in maximized mode
			options.addArguments("disable-infobars"); // disabling infobars
			options.addArguments("--disable-extensions"); // disabling extensions
			options.addArguments("--disable-gpu"); // applicable to windows os only
			options.addArguments("--no-sandbox"); // Bypass OS security model
	        driver = new ChromeDriver(options);
			WaitStatement.iSleep(2);
			
	        String environemnt = System.getProperty("environment"); //here you read your environment name
            System.out.println("getttttttttttttttt"); 
	        System.out.println(environemnt);
			
	        ConfigFactory.setProperty("env", environment);
              System.out.println("settttttttttttttt");
	        testEnvironment = ConfigFactory.create(Environment.class);
//              InputStream inputStream;
// 		     Properties prop=new Properties();
// 		    prop.load(getClass().getResourceAsStream("/home/swati/eclipse-workspace/InvestwellOnline/config/myconfig.properties"));

// 	    	inputStream = getClass().getClassLoader().getResourceAsStream("/home/swati/eclipse-workspace/InvestwellOnline/config/myconfig.properties");
// 	    	if (inputStream != null) {
// 				prop.load(inputStream);
// 			} else {
// 				throw new FileNotFoundException("property file '"  + "' not found in the classpath");
// 			}
// 	    	String url = prop.getProperty("url");
// 			String username = prop.getProperty("username");
// 			String password = prop.getProperty("password");
	        System.out.println(testEnvironment.url());

	       System.out.println("dfghhgfdfghnhgfg");
	       try {
	    	driver.get(testEnvironment.url());
	       }catch (Exception e) {
	           // TODO: handle exception
	           e.printStackTrace();
	       }
	    	System.out.println("nextghjhgfdgh");
//	    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			WaitStatement.iSleep(2);
			Login_Page_Elements loginPage= new Login_Page_Elements(driver);

			loginPage.login_prod(testEnvironment.username(),testEnvironment.password());

			System.out.println("---------------Broker Script Running-----------");
			
		}
		
		@AfterTest
		public void after_test_setup()
		{
			driver.close();
//			driver.quit();

			System.out.println("---------------Broker Script End-----------");
		}
		
		@AfterMethod
		public void aftertestsetup(ITestResult result) throws Exception
		{
			
			String ScriptName = result.getMethod().getMethodName();
			if(result.isSuccess())
			{
				Reporter.log(ScriptName+" Script is passed :", true);
			}
			else
			{
				Reporter.log(ScriptName+" Script is failed :", true);
				ScreenshotLib.takescreenshot(driver, ScriptName);
				Reporter.log("SCreenshot has been taken", true);			
			}
			
		}
			
		
		@BeforeMethod
		public void beforetestsetup(Method method)
		{

			System.out.println("before method started");
			
//			driver.get(baseurl);	
//			loginPage.login_prod(username, password);
			String methodname=method.getName();
			
//			test=report.createTest(methodname);
			
		}

//		
//		public String url() {
//			// TODO Auto-generated method stub
//			return this.url();
//		}
//
//		public String username() {
//			// TODO Auto-generated method stub
//			return this.username();
//		}
//
//		
//		public String password() {
//			// TODO Auto-generated method stub
//			return this.password();
//		}
//
//		public String getDBHostname() {
//			// TODO Auto-generated method stub
//			return this.getDBHostname();
//		}
//
//		
//		public String getDBUsername() {
//			// TODO Auto-generated method stub
//			return this.getDBUsername();
//		}
//
//		public String getDBPassword() {
//			// TODO Auto-generated method stub
//			return this.getDBPassword();
//		}
//		
		
	
}
