package Page_Elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Functions.Global_Methods;
import Generic.BaseLib;


public class Login_Page_Elements extends BaseLib
{

	@FindBy(xpath="//input[@type='text']")	private WebElement username;
	@FindBy(xpath="//input[@type='password']")	private WebElement password;
	@FindBy(xpath="//input[@type='submit']")	private WebElement login;
	
	Global_Methods gm = new Global_Methods(driver);
	
	public Login_Page_Elements(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void login_prod(String userName1, String pASSWORD1) {
		username.sendKeys(userName1);
//		gm.sendkey(login, userName1);
		password.sendKeys(pASSWORD1);
		login.click();
	}
	
	
}
