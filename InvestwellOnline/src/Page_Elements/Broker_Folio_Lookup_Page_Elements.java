package Page_Elements;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import Functions.Global_Methods;
import Generic.BaseLib;
import Generic.DatabaseConfig;
import Generic.WaitStatement;

public class Broker_Folio_Lookup_Page_Elements extends BaseLib
{
//	public WebDriver driver;
	Global_Methods gm = new Global_Methods(driver);
	DatabaseConfig db= new DatabaseConfig();
	SoftAssert soft=new SoftAssert();
	Actions act=new Actions(driver);
	WaitStatement WaitStatement=new WaitStatement(driver);

	
	public Broker_Folio_Lookup_Page_Elements(WebDriver driver) {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath="//span[@class='close']")private WebElement Filter_Close_Button;
	@FindBy(xpath = "//div[@class='profile-header']/h2[1]")private WebElement header_text;
	@FindBy(xpath = "//div[@class='btnsContainer']/button")private WebElement applyfilterbtn;
	@FindBy(xpath="//ul[@class='defineCols']/li[2]/div/span")private WebElement switchbtn1;
	@FindBy(xpath="//ul[@class='defineCols']/li[3]/div")private WebElement switchbtn2;

	@FindBy(xpath = "//table/thead/tr/th/span[1]")private List<WebElement> FolioLookup_Table_Header;
	@FindBy(xpath = "//tbody/tr[1]/td[2]/span[1]")private WebElement First_Folio;
	@FindBy(xpath = "//tbody/tr/td[2]/span[1]")private List<WebElement> FolioRow;
	@FindBy(xpath = "//input[@placeholder='Search Folio']")private WebElement folio_search;
	@FindBy(xpath = "//a[text()='Report Configuration']")private WebElement reportconf;
	@FindBy(xpath = "//table/thead/tr/th[5]/span[2]")private WebElement scheme_search;
	@FindBy(xpath = "//input[@placeholder='Scheme Name ']")private WebElement schemePlace;
	@FindBy(xpath = "//tbody/tr[1]/td[5]/span[1]")private WebElement first_scheme;
	@FindBy(xpath = "//tbody/tr/td[5]/span[1]")private List<WebElement> scheme_row;
	@FindBy(xpath = "//input[@placeholder='Search Applicant']")private WebElement investor_folio;
	@FindBy(xpath = "//input[@placeholder='Search Applicant']")private WebElement applicant_folio;
	@FindBy(xpath = "//tbody/tr[1]/td[6]/span[1]")private WebElement First_Investor;
	@FindBy(xpath="//tbody/tr[2]/td[6]/span[1]")private WebElement Second_Investor;

	@FindBy(xpath = "//tbody/tr/td[6]/span")private List<WebElement> ApplicantRow;
	@FindBy(xpath = "//table/thead/tr/th[7]/span[2]")private WebElement applicantSearch_mapped;
	@FindBy(xpath = "//input[@placeholder='Applicant (Mapped To)']")
	private WebElement applicantmapped_place;
	@FindBy(xpath = "//tbody/tr[1]/td[7]/span")
	private WebElement First_ApplicantMapped;
	@FindBy(xpath = "//tbody/tr/td[7]/span")
	private List<WebElement> InvestorRow_MappedTo;
	@FindBy(xpath = "//tbody/tr[1]/td[8]/span[1]")
	private WebElement ApplicantPAN;
	@FindBy(xpath = "//table/thead/tr/th[8]/span[2]")
	private WebElement ApplicantPANSearch;
	@FindBy(xpath = "//input[@placeholder='PAN']")
	private WebElement ApplicantPANField;
	@FindBy(xpath = "//tbody/tr/td[8]/span[1]")
	private List<WebElement> AppilcantPANRow;
	@FindBy(xpath="//table/thead/tr/th[8]/span[1]")private WebElement PAN;
	@FindBy(xpath = "//table/thead/tr/th[11]/span[1]")
	private WebElement iincanucc;
	@FindBy(xpath="//div[@class='tableHeadScroll']")	private WebElement Horizontalscroller;
	@FindBy(xpath = "//tbody/tr[1]/td[11]/span[1]")
	private WebElement firstiincanucc;
	@FindBy(xpath = "//table/thead/tr/th[11]/span[2]")
	private WebElement iincanuccSearch;
	@FindBy(xpath = "//input[@placeholder='CAN Number' or @placeholder='UCC Number' or @placeholder='IIN Number']")
	private WebElement iincanuccField;
	@FindBy(xpath = "//tbody/tr/td[11]/span[1]")
	private List<WebElement> iincanuccRow;
	
	@FindBy(xpath = "//table/thead/tr/th[12]/span[1]")
	private WebElement TaxStatus;
	@FindBy(xpath = "//tbody/tr[1]/td[12]/span[1]")
	private WebElement firstTaxStatus;
	@FindBy(xpath = "//table/thead/tr/th[12]/span[2]")
	private WebElement taxStatusSearch;
	@FindBy(xpath = "//input[@placeholder='Tax Status']")
	private WebElement taxStatusField;
	@FindBy(xpath = "//tbody/tr/td[12]/span[1]")
	private List<WebElement> taxStatusRow;
	
	@FindBy(xpath = "//table/thead/tr/th[13]/span[1]")
	private WebElement Holding;
	@FindBy(xpath = "//tbody/tr[1]/td[13]/span[1]")
	private WebElement firstHolding;
	@FindBy(xpath = "//table/thead/tr/th[13]/span[2]")
	private WebElement HoldingSearch;
	@FindBy(xpath="//tbody/tr/td[13]/span[1]")private List<WebElement> holdingRow;
	@FindBy(xpath="//input[@placeholder='Mode of Holding ']")private WebElement HoldingField;
	
	@FindBy(xpath = "//table/thead/tr/th[14]/span[1]")
	private WebElement Joint1Name;
	@FindBy(xpath = "//tbody/tr[1]/td[14]/span[1]")
	private WebElement firstJoint1Name;
	@FindBy(xpath = "//table/thead/tr/th[14]/span[2]")
	private WebElement Joint1NameSearch;
	@FindBy(xpath="//tbody/tr/td[14]/span[1]")private List<WebElement> Joint1NameRow;
	@FindBy(xpath="//input[@placeholder='Joint 1 Name']") private WebElement Joint1NameField;
	
	@FindBy(xpath = "//table/thead/tr/th[15]/span[1]")
	private WebElement Joint1PAN;
	@FindBy(xpath = "//tbody/tr[1]/td[15]/span[1]")
	private WebElement firstJoint1PAN;
	@FindBy(xpath = "//table/thead/tr/th[15]/span[2]")
	private WebElement Joint1PANSearch;
	@FindBy(xpath="//tbody/tr/td[15]/span[1]")private List<WebElement> Joint1PanRow;
	@FindBy(xpath="//input[@placeholder='Joint 1 PAN']")private WebElement Joint1PanField;

	@FindBy(xpath = "//table/thead/tr/th[16]/span[1]")
	private WebElement Joint2Name;
	@FindBy(xpath = "//tbody/tr[1]/td[16]/span[1]")
	private WebElement firstJoint2Name;
	@FindBy(xpath = "//table/thead/tr/th[16]/span[2]")
	private WebElement Joint2NameSearch;	
	@FindBy(xpath="//tbody/tr/td[16]/span[1]")private List<WebElement> Joint2NameRow ;
	@FindBy(xpath="//input[@placeholder='Joint 2 Name']")private WebElement Joint2Namefield ;

	@FindBy(xpath = "//table/thead/tr/th[17]/span[1]")
	private WebElement Joint2PAN;
	@FindBy(xpath = "//tbody/tr[1]/td[17]/span[1]")
	private WebElement firstJoint2PAN;
	@FindBy(xpath = "//table/thead/tr/th[17]/span[2]")
	private WebElement Joint2PanSearch;
	@FindBy(xpath="//tbody/tr/td[17]/span[1]")private List<WebElement> Joint2PanRow;
	@FindBy(xpath="//input[@placeholder='Joint 2 PAN']")private WebElement Joint2Panfield ;

	@FindBy(xpath = "//table/thead/tr/th[18]/span[1]")
	private WebElement GuardianName;
	@FindBy(xpath = "//tbody/tr[1]/td[18]/span[1]")
	private WebElement firstGuardianName;
	@FindBy(xpath = "//table/thead/tr/th[18]/span[2]")
	private WebElement GuardianNameSearch;
	@FindBy(xpath="//tbody/tr/td[18]/span[1]")private List<WebElement> GuardianNameRow;
	@FindBy(xpath="//input[@placeholder='Guardian Name']")private WebElement GuardianNameField;

	@FindBy(xpath = "//table/thead/tr/th[19]/span[1]")
	private WebElement GuardianPAN;
	@FindBy(xpath = "//tbody/tr[1]/td[19]/span[1]")
	private WebElement firstGuardianPAN;
	@FindBy(xpath = "//table/thead/tr/th[19]/span[2]")
	private WebElement GuardianPANSearch;
	@FindBy(xpath="//tbody/tr/td[19]/span[1]")private List<WebElement> GuardianPANRow;
	@FindBy(xpath="//input[@placeholder='Guardian PAN']")private WebElement GuardianPANfield ;
	
	
	@FindBy(xpath="//tbody/tr[1]/td[1]//input[@type='checkbox']/..")private WebElement First_Checkbox ;
	@FindBy(xpath="//tbody/tr[2]/td[1]//input[@type='checkbox']/..")private WebElement Second_Checkbox ;
	@FindBy(xpath="//div[@class='pagination rightSide']/ul/li[2]")private WebElement SecondPAge;
	@FindBy(xpath="//div[@class='pagination rightSide']/ul/li[1]")private WebElement FirstPage;
	
	@FindBy(xpath="//tbody/tr/td[9]/span")private List<WebElement> Amount_row;
	@FindBy(xpath="//tbody/tr/td[9]")private List<WebElement> Amount_row_alignment;
	@FindBy(xpath="//div[@class='backTableContent']/div[1]")private WebElement Header_popup_text;
	@FindBy(xpath="//div[@class='tableTransparent boxWhiteBg']/div/div[1]/ul/li[3]/span[2]")private WebElement Detail_Scheme;
	@FindBy(xpath="//div[@class='tableTransparent boxWhiteBg']/table/tr[1]/td[4]")private WebElement Detail_Pan;
	@FindBy(xpath="//div[@class='tableTransparent boxWhiteBg']/div/div[2]/ul/li[1]/span[2]")private WebElement Detail_Mappedto;
	@FindBy(xpath="//div[@class='tableTransparent boxWhiteBg']/div/div[1]/ul/li[1]/span[2]")private WebElement Detail_InvestorName;
	@FindBy(xpath="//div[@class='tableTransparent boxWhiteBg']/div/div[1]/ul/li[2]/span[2]")private WebElement Detail_folio_no;
	@FindBy(xpath="//tbody/tr[1]/td[3]")private WebElement First_SOA;
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec boxWhiteBg']/div[1]/div[1]")private WebElement txnsliplblSOA;
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec boxWhiteBg']/div[1]/div[2]/div/div/p/span")private WebElement txnslipschemenameSOA;
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec boxWhiteBg']/div[1]/div[4]/button")private WebElement txnslipbtnSOA;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[2]/div/div/table/tbody/tr/td[5]")private WebElement SOA_NAV;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[2]/div/div/table/thead/tr/th/span")private List<WebElement> SOA_Table_Header;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div/div[1]/table[2]/tr/td")private List <WebElement> SOA_Table_HeadUp_Values;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[1]/table[1]/tr[1]/td[2]")private WebElement SOA_Name;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[1]/table[1]/tr[2]/td[2]")private WebElement SOA_Scheme;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[1]/table[1]/tr[1]/td[4]")private WebElement SOA_Folio;
	@FindBy(xpath="//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[2]/div/div/table/tbody/tr/td[4]")private List<WebElement> SOA_AmountRow;
	
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec']/div[1]/div[1]")private WebElement txnslipschemelabel;
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec']/div[1]/div[2]/div/div/p/span")private WebElement txnslipschemename;
	@FindBy(xpath="//div[@class='posRelative filterArea customBtmSec']/div[1]/div[4]/button")private WebElement txndownloadbtn;
	@FindBy(xpath="//div[@class='selectOpts']/div/div/ul/li[1]/input")private WebElement searchscheme;
	@FindBy(xpath="//div[@class='selectOpts']/div/div/ul/li[2]/ul/li[1]")private WebElement firstscheme_txnslip;

	@FindBy(xpath="//p[@title='Choose Action']")private WebElement Choose_Action;
	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined' and span[text()='Action:']]/following-sibling::ul/li/ul/li")private List<WebElement> Folio_lookup_All_Actions;
	@FindBy(xpath="//a[text()='Proceed']")private WebElement Proceed_Button;



	//	@FindBy(xpath="")private WebElement ;
	
	

	public void click_FolioLookup() {
		
		driver.get("http://demo.investwellonline.com/app/#/broker/folioLookup");
		WaitStatement.iWaitForSecs(20);

	}
	

	public void Header_text_verify() {
		String actual = header_text.getText();
		String expected = "Folio Lookup";
		soft.assertEquals(actual, expected);
		soft.assertAll();
	}

	public void Verify_Table_Header() {
		boolean button = applyfilterbtn.isDisplayed();
		System.out.println(button);
		if (button == true) {
			applyfilterbtn.click();
		}
		
		gm.screen_resolution();

		for (WebElement e : FolioLookup_Table_Header) {
			String p = e.getText();
			System.out.println("sdfgbhgfd");
			System.out.println(p);
			if (p.contains("UCC Number")) {
				System.out.println("BSE");
				soft.assertTrue(p.equals("Select") | p.equals("SOA") | p.equals("Folio") | p.equals("Freeze Date")
						| p.equals("Scheme Name") | p.equals(" search") | p.equals("Applicant (Folio)")
						| p.equals("Applicant (Mapped To)") | p.equals(" search") | p.equals("PAN")
						| p.equals(" search") | p.startsWith("Amount") | p.equals("FT Folio") | p.equals("UCC Number")
						| p.equals(" search") | p.equals("Tax Status") | p.equals("Mode of Holding")
						| p.equals("Joint 1 Name") | p.equals("Joint 1 PAN") | p.equals("Joint 2 Name")
						| p.equals("Joint 2 PAN") | p.equals("Guardian Name") | p.equals("Guardian PAN"),
						"Foolio Lookup Table Header Value MisMatch:: " + p);
				soft.assertAll();
			} else if (p.contains("IIN Number")) {
				System.out.println("NSE");
				soft.assertTrue(
						p.equals("Select") && p.equals("SOA") && p.equals("Folio") && p.equals("Freeze Date")
								&& p.equals("Scheme Name") && p.equals("Applicant (Folio)")
								&& p.equals("Applicant (Mapped To)") && p.equals("PAN")
								&& p.startsWith("Amount") &&p.equals("FT Folio") && p.startsWith("IIN Number")
							 && p.equals("Tax Status") && p.equals("Mode of Holding")
								&& p.equals("Joint 1 Name") && p.equals("Joint 1 PAN")&& p.equals("Joint 2 Name")
								
								&& p.equals("Joint 2 PAN") && p.equals("Guardian Name") && p.equals("Guardian PAN"),
						"Foolio Lookup Table Header Value MisMatch:: " + p);
				soft.assertAll();
			} else if (p.contains("CAN Number")) {
				System.out.println("MFU");
				soft.assertTrue(
						p.equals("Select") && p.equals("SOA") && p.equals("Folio") && p.equals("Freeze Date")
								&& p.equals("Scheme Name") && p.equals("Applicant (Folio)")
								&& p.equals("Applicant (Mapped To)") && p.equals("PAN")
								&& p.startsWith("Amount") &&p.equals("FT Folio") && p.startsWith("CAN Number")
							    && p.equals("Tax Status") && p.equals("Mode of Holding")
								&& p.equals("Joint 1 Name") && p.equals("Joint 1 PAN")&& p.equals("Joint 2 Name")
							    && p.equals("Joint 2 PAN") && p.equals("Guardian Name") && p.equals("Guardian PAN"),
						"Foolio Lookup Table Header Value MisMatch:: " + p);
				soft.assertAll();
			}

		}
	}

	public void Verify_SearchFolio_Functionality() {
		applyfilterbtn.click();
		WaitStatement.iSleep(2);
		String folio = First_Folio.getText();
		reportconf.click();
		WaitStatement.iSleep(2);
		folio_search.click();
		folio_search.sendKeys(folio);
		applyfilterbtn.click();
		WaitStatement.iSleep(2);
		for (int i = 0; i < FolioRow.size(); i++) {
			WebElement e = FolioRow.get(i);
			String folios = e.getText();
			System.out.println(folios);
			soft.assertTrue(folios.equals(folio), "Search Functionality is not working with Folio");
			soft.assertAll();
		}
	}
	
	public void Verify_SearchInvestor_Functionality() 
	{
		SoftAssert sa=new SoftAssert();
		applyfilterbtn.click();
		WaitStatement.iSleep(2);
		String investor=First_Investor.getText();
		String [] str=investor.split(" ");
		String newinvestor=str[0].toLowerCase();
		reportconf.click();
		WaitStatement.iSleep(2);
//		driver.findElement(Search_box).sendKeys(str[1]);
		investor_folio.click();
		investor_folio.sendKeys(investor);
//		driver.findElement(applicant_folio).sendKeys(Keys.ENTER);
		applyfilterbtn.click();

		WaitStatement.iSleep(2);
		for(int i=0;i<ApplicantRow.size();i++)
		{
			WebElement e=ApplicantRow.get(i);
			String investors=e.getText();
			System.out.println(newinvestor);
			System.out.println(investors);
			soft.assertTrue(investors.contains(str[0])|investors.contains(newinvestor), "Search Functionality is not working with Applicant");
			soft.assertAll();
			return;
		}
	}
	
	public void ApplyFilter()
	{
		applyfilterbtn.click();
		WaitStatement.iSleep(2);
	}
	
	
	public void Verify_SearchScheme_Functionality() 
	{
		WaitStatement.iSleep(2);
		String investor=first_scheme.getText();
		String [] str=investor.split(" ");
		String newinvestor=str[0].toLowerCase();
		
		scheme_search.click();
	    schemePlace.sendKeys(investor);
		schemePlace.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		
		for(int i=0;i<scheme_row.size();i++)
		{
			WebElement e=scheme_row.get(i);
			String investors=e.getText();
			System.out.println(newinvestor);
			System.out.println(investors);
			soft.assertTrue(investors.contains(str[0])|investors.contains(newinvestor), "Search Functionality is not working with Scheme");
			soft.assertAll();
			
		}
		
		
	}

	
	
	
	
	public  void Verify_SearchApplicant_Mapped() 
	{
		System.out.println("sdsdsd");
//		applyfilterbtn.click();
		Filter_Close_Button.click();
		System.out.println("lllllllllllll");
		WaitStatement.iSleep(4);
		String investor=First_ApplicantMapped.getText();
		String [] str=investor.split(" ");
		String newinvestor=str[0].toLowerCase();
		WaitStatement.iSleep(2);
//		driver.findElement(Search_box).sendKeys(str[1]);
		applicantSearch_mapped.click();
		applicantmapped_place.sendKeys(investor);
		applicantmapped_place.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		for(int i=0;i<InvestorRow_MappedTo.size();i++)
		{
			WebElement e=InvestorRow_MappedTo.get(i);
			String investors=e.getText();
			System.out.println(newinvestor);
			System.out.println(investors);
			soft.assertTrue(investors.contains(str[0])|investors.contains(newinvestor), "Search Functionality is not working with Applicant");
			soft.assertAll();
			return;
		}
		
	}
	
	public void Verify_SearchPAN() 
	{
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		String pan=ApplicantPAN.getText();
		ApplicantPANSearch.click();
		ApplicantPANField.sendKeys(pan);
		ApplicantPANField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		for(int i=1;i<AppilcantPANRow.size();i++) 
		{
			WebElement e=AppilcantPANRow.get(i);
			String clientPAN=e.getText();
			System.out.println("searched PAN : "+pan);
			System.out.println("Client PAN: "+clientPAN);
			soft.assertTrue(clientPAN.contains(pan),"Search Pan functionality is not working.");
			soft.assertAll();
			
		}
		
	}
	
	
	public void Verify_IINCANUCCSearch() 
	{
		Filter_Close_Button.click();
		gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
		WaitStatement.iSleep(2);
		iincanucc.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
		WaitStatement.iSleep(2);
//		gm.screen_resolution(driver);
		String Number=firstiincanucc.getText();
		iincanuccSearch.click();
		iincanuccField.sendKeys(Number);
		iincanuccField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
		WaitStatement.iSleep(2);
		
		for(int i=1;i<iincanuccRow.size();i++)
		{
			WebElement e=iincanuccRow.get(i);
			String numbers=e.getText();
			System.out.println("number: "+numbers);
			soft.assertTrue(numbers.contains(Number),"Identify numbers search is not working");
			soft.assertAll();
		}
		
		
	}


	public void Verify_TexStatusSearch() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, taxStatusSearch);
		taxStatusSearch.click();
		taxStatusField.sendKeys("Individual");
		taxStatusField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, taxStatusSearch);
		WaitStatement.iSleep(2);

		for(int i=1;i<taxStatusRow.size();i++)
		{
			WebElement e=taxStatusRow.get(i);
			String taxStatusresult=e.getText();
			soft.assertTrue(taxStatusresult.contains("Individual"),"Tax status search functionality is not working.");
			soft.assertAll();
			
		}
		// TODO Auto-generated method stub
		
	}


	public void Verify_ModeOfHolding() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
        gm.Horizontal_scroll(Horizontalscroller, HoldingSearch);
		WaitStatement.iSleep(2);

		HoldingSearch.click();
		HoldingField.sendKeys("SI");
		HoldingField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, HoldingSearch);
		WaitStatement.iSleep(2);
		for(int i=1;i<holdingRow.size();i++)
		{
			WebElement e=holdingRow.get(i);
			String holdingResult=e.getText();
			soft.assertTrue(holdingResult.contains("SI"),"Holding serach functionality is not working");
			soft.assertAll();
		}
		// TODO Auto-generated method stub
		
	}


	public void Verify_Joint1Name() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint1NameSearch);
		WaitStatement.iSleep(2);
		Joint1Name.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint1NameSearch);
		WaitStatement.iSleep(2);
		Joint1Name.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint1NameSearch);
		WaitStatement.iSleep(2);
		String Joint1Client=firstJoint1Name.getText();
		Joint1NameSearch.click();
		Joint1NameField.sendKeys(Joint1Client);
		Joint1NameField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint1NameSearch);
		WaitStatement.iSleep(2);

		 for(int i=1;i<Joint1NameRow.size();i++)
		 {
			 WebElement e=Joint1NameRow.get(i);
			 String Joint1ClientResult=e.getText();
			 soft.assertTrue(Joint1ClientResult.contains(Joint1Client),"Joint1 client search is not working");
			 soft.assertAll();
		 }
		// TODO Auto-generated method stub
		
	}


	public void Verify_Joint1PAN() {
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
        gm.Horizontal_scroll(Horizontalscroller, Joint1PANSearch);
		WaitStatement.iSleep(2);
		String Joint1ClientPAN=firstJoint1PAN.getText();
		Joint1PANSearch.click();
		Joint1PanField.sendKeys(Joint1ClientPAN);
		Joint1PanField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint1PANSearch);
		WaitStatement.iSleep(2);

		for(int i=1;i<Joint1PanRow.size();i++)
		{
			 WebElement e=Joint1PanRow.get(i);
			 String Joint1ClientPANResult=e.getText();
			 soft.assertTrue(Joint1ClientPANResult.contains(Joint1ClientPAN),"Joint1 client PAN search is not working");
			 soft.assertAll();
		}
		
		// TODO Auto-generated method stub
		
	}


	public void Verify_Joint2Name() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint2NameSearch);
		WaitStatement.iSleep(2);
		Joint2Name.click();
		WaitStatement.iSleep(2);

		Joint2Name.click();
		WaitStatement.iSleep(2);

		gm.Horizontal_scroll(Horizontalscroller, Joint2NameSearch);
		WaitStatement.iSleep(2);
		String Joint2Client=firstJoint2Name.getText();
		Joint2NameSearch.click();
		Joint2Namefield.sendKeys(Joint2Client);
		Joint2Namefield.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint2NameSearch);
		WaitStatement.iSleep(2);

		 for(int i=1;i<Joint2NameRow.size();i++)
		 {
			 WebElement e=Joint2NameRow.get(i);
			 String Joint2ClientResult=e.getText();
			 soft.assertTrue(Joint2ClientResult.contains(Joint2Client),"Joint2 client search is not working");
			 soft.assertAll();
		 }
		// TODO Auto-generated method stub
		
	}


	public void Verify_Joint2PAN() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint2PanSearch);
		WaitStatement.iSleep(2);
		String Joint2ClientPAN=firstJoint2PAN.getText();
		Joint2PanSearch.click();

		Joint2Panfield.sendKeys(Joint2ClientPAN);
		Joint2Panfield.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, Joint2PanSearch);
		WaitStatement.iSleep(2);
		for(int i=1;i<Joint2PanRow.size();i++)
		{
			 WebElement e=Joint2PanRow.get(i);
			 String Joint2ClientPANResult=e.getText();
			 soft.assertTrue(Joint2ClientPANResult.contains(Joint2ClientPAN),"Joint2 client PAN search is not working");
			 soft.assertAll();
		}
		// TODO Auto-generated method stub
		
	}


	public void Verify_GuardianName() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, GuardianNameSearch);
		WaitStatement.iSleep(2);
		GuardianName.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, GuardianNameSearch);
		WaitStatement.iSleep(2);
		String GuardianClient=firstGuardianName.getText();
		GuardianNameSearch.click();
		GuardianNameField.sendKeys(GuardianClient);
		GuardianNameField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, GuardianNameSearch);
		WaitStatement.iSleep(2);

		 for(int i=1;i<GuardianNameRow.size();i++)
		 {
			 WebElement e=GuardianNameRow.get(i);
			 String GuardianClientResult=e.getText();
			 soft.assertTrue(GuardianClientResult.contains(GuardianClient),"Guardian client Name search is not working");
			 soft.assertAll();
		 }
		// TODO Auto-generated method stub
		
	}


	public void Verify_GuardianPAN() {
		
		Filter_Close_Button.click();
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, GuardianPANSearch);
		WaitStatement.iSleep(2);
		String GuardianPANClient=firstGuardianPAN.getText();
		GuardianPANSearch.click();
		GuardianPANfield.sendKeys(GuardianPANClient);
		GuardianPANfield.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(2);
		gm.Horizontal_scroll(Horizontalscroller, GuardianPANSearch);
		WaitStatement.iSleep(2);

		 for(int i=1;i<GuardianPANRow.size();i++)
		 {
			 WebElement e=GuardianPANRow.get(i);
			 String GuardianPANClientResult=e.getText();
			 soft.assertTrue(GuardianPANClientResult.contains(GuardianPANClient),"Guardian PAN client search is not working");
			 soft.assertAll();
		 }
		// TODO Auto-generated method stub
		
	}


	public void Verify_Brief()  {
		
		switchbtn1.click();
		applyfilterbtn.click();
		WaitStatement.iSleep(2);
		
		
	}
	
	
	public void	CommaSeperatedValues() 
	{
		
		WaitStatement.iSleep(2);
		for(int i=0;i<Amount_row.size();i++)
		{
			WebElement e=Amount_row.get(i);
			String a=e.getText();
			int length=a.length();
			if(length>=4)
			{
				a.contains(",");
				soft.assertTrue(a.contains(","), "Comma Seperated values is not present");
				soft.assertAll();
			}
		}
	}
	
	public void Verify_AmountRow_Right_Aligned()
	{
		for(int i=0;i<Amount_row_alignment.size();i++)
		{
			WebElement e=Amount_row_alignment.get(i);
			String str=e.getAttribute("class");
			soft.assertTrue(str.equals("right-align"), "Amount Row is not right-align");
			soft.assertAll();
		}
	}

	
	public void Verify_FolioDetails() 
	{
		String schemename=first_scheme.getText();
		String InvestorFolio=First_Investor.getText();
		String InvestotMappedto=First_ApplicantMapped.getText();
		String Folio=First_Folio.getText();
		
		First_Folio.click();

		String foliodetails=Header_popup_text.getText();
		String detail_scheme=Detail_Scheme.getText();	
		String detail_investorfolio=Detail_InvestorName.getText();	
		System.out.println(detail_investorfolio);
		String detail_Mappedto=Detail_Mappedto.getText();
		System.out.println(detail_Mappedto);
		
		String folio_no=Detail_folio_no.getText();
		soft.assertTrue(detail_investorfolio.contains(InvestorFolio)&detail_Mappedto.contains(InvestotMappedto)&foliodetails.equals("Folio Detail")&schemename.equals(detail_scheme)&Folio.equals(folio_no),  "Folio Details are not matching");
		
		soft.assertAll();
		
	}
	
	String downloadPath = "/home/swati/Downloads";

	public void Verify_Download_Txnslip_FolioDetails() 
	{

		String applicantname=First_Investor.getText();
		String schemename=first_scheme.getText();

		First_Folio.click();

		String txnslip=txnslipschemelabel.getText();
		String txnslipscheme=txnslipschemename.getText();
		
		WaitStatement.iSleep(3);
		soft.assertTrue(txnslip.equals("Transaction Slip Scheme :")&schemename.equals(txnslipscheme), "Folio detail are not matching");
		soft.assertTrue(txndownloadbtn.isDisplayed(), "Download button didn't displayed");
		
		txndownloadbtn.click();
		WaitStatement.iSleep(5);
		File getLatestFile = gm.getLatestFilefromDir(downloadPath);
		String fileName = getLatestFile.getName();
		String fileName2=fileName.replace("_", "");
		System.out.println(fileName2);
		soft.assertTrue(fileName2.contains(applicantname), "Downloaded file name is not matching with expected file name");
		WaitStatement.iSleep(5);
	    act.moveToElement(txnslipschemename).build().perform();
	    txnslipschemename.click();
	    searchscheme.sendKeys("b");
	    act.moveToElement(firstscheme_txnslip).build().perform();
	    firstscheme_txnslip.click();
		WaitStatement.iSleep(5);
	    txndownloadbtn.click();
		WaitStatement.iSleep(5);
		File getLatestFile2 = gm.getLatestFilefromDir(downloadPath);
		String filenname = getLatestFile2.getName();
		String filenname2=filenname.replace("_", "");
		System.out.println(filenname2);
		soft.assertTrue(filenname2.contains(applicantname), "Downloaded file name is not matching with expected file name");
		
		soft.assertAll();
		
	}
	
	
	public void Verify_Download_Txnslip_SOA() 
	{
		String SOAinvestor=First_Investor.getText();
		String SOAscheme=first_scheme.getText();
		First_SOA.click();
		WaitStatement.iSleep(2);
		
		String txnslip=txnsliplblSOA.getText();
		String txnslipscheme=txnslipschemenameSOA.getText();
		
		WaitStatement.iSleep(5);
		soft.assertTrue(txnslip.equals("Transaction Slip Scheme :")&SOAscheme.equals(txnslipscheme), "SOA detail are not matching");
		soft.assertTrue(txnslipbtnSOA.isDisplayed(), "Download button didn't displayed");
		
		txnslipbtnSOA.click();
		WaitStatement.iSleep(5);
		File getLatestFile = gm.getLatestFilefromDir(downloadPath);
		String fileName = getLatestFile.getName();
		String fileName2=fileName.replace("_", "");
		System.out.println(fileName2);
		soft.assertTrue(fileName2.contains(SOAinvestor), "Downloaded file name is not matching with expected file name");
		WaitStatement.iSleep(5);
	    act.moveToElement(txnslipschemenameSOA).build().perform();
	    txnslipschemenameSOA.click();
	    searchscheme.sendKeys("b");
	    act.moveToElement(firstscheme_txnslip).build().perform();
	    firstscheme_txnslip.click();
		WaitStatement.iSleep(5);
	    txnslipbtnSOA.click();
		WaitStatement.iSleep(5);
		File getLatestFile2 = gm.getLatestFilefromDir(downloadPath);
		String filenname = getLatestFile2.getName();
		String filenname2=filenname.replace("_", "");
		System.out.println(filenname2);
		soft.assertTrue(filenname2.contains(SOAinvestor), "Downloaded file name is not matching with expected file name");
		
		soft.assertAll();
		
		
	}
	
	public void Verify_SOA_values2() 
	{

		String mappedto=First_ApplicantMapped.getText();
		String folio=First_Folio.getText();
		String scheme=first_scheme.getText();
		
		First_SOA.click();
		WaitStatement.iSleep(5);
		String SOAinvestor=SOA_Name.getText();
		String SOAfolio=SOA_Folio.getText();
		String SOAscheme=SOA_Scheme.getText();
		soft.assertTrue(mappedto.equals(SOAinvestor)&folio.equals(SOAfolio)&scheme.equals(SOAscheme), "SOA Values are not correct");
		soft.assertAll();
	}
	
	public void Verify_SOA_2Decimal_For_Amount_Row()
	{
		gm.scroll_page_verticall(250);
		WaitStatement.iSleep(2);
		for(WebElement e:SOA_AmountRow)
		{
			String p=e.getText();
			if(p!="0")
			{
				String []str=p.split("\\.");
				int l=str[1].length();
				soft.assertTrue(l==2, "Decimal Points are not correct which should be 2");
				soft.assertAll();
			}
		}
	}
	
	public void Verify_SOA_4Decimal_For_NAV_Unit_Balance_Rows()
	{
		
		for(int n=5;n<8;n++)
		{
		List<WebElement> ele=driver.findElements(By.xpath("//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[2]/div/div/table/tbody/tr/td["+n+"]"));
		for(int i=0;i<ele.size();i++)
		{
			WebElement e=ele.get(i);
			String p=e.getText();
			if(p.length()>1)
			{
				String []str=p.split("\\.");
				int l=str[1].length();
				soft.assertTrue(l==4, "Decimal Points are not correct which should be 4:::"+l+"");
				soft.assertAll();
			}
		}
		}
	}
	
	
	public void Verify_SOA_AmountRow_CommaSeparated()
	{
		for(int n=4;n<5;n++)
		{
		List<WebElement> ele=driver.findElements(By.xpath("//div[@class='tableListing boxShadow tableShadowOff optionsTable tableOnOverlay ']/div[2]/div[2]/div/div/table/tbody/tr/td["+n+"]"));
			for(int i=0;i<ele.size();i++)
			{
				WebElement e=ele.get(i);
				String p=e.getText();
				String str[]=p.split("\\.");
				
				int len=str[0].length();
				if (len>=  4)
				{
					soft.assertTrue(str[0].contains(","),"SOA Columns didn't have comma separated values");
					soft.assertAll();
					break;
				}
			}
		}
	}	

//action functionality==============================================================
	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined' and span[text()='Action:']]/following-sibling::ul/li/ul/li[1]")private WebElement assign_folio;
	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined' and span[text()='Action:']]/following-sibling::ul/li/ul/li[2]")private WebElement demerge_client;
	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined' and span[text()='Action:']]/following-sibling::ul/li/ul/li[3]")private WebElement update_number;

	@FindBy(xpath="//span[text()='Assign Folio to Existing Applicant']")private WebElement assign_folio_investor_text;
	@FindBy(xpath="//input[@class='text-field-filter' and @id='selectInput']")private WebElement Choose_Search;
	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined']/following::ul/li[2]/ul/li[1]")private WebElement Choose_FirstRecordAfterSearch;
	@FindBy(xpath="//a[text()='Assign Folio']")private WebElement Assign_Folio_Button;

	@FindBy(xpath="//a[text()='Create And Assign ']")private WebElement create_assign;

	
	public void Verify_Folio_Lookup_Actions() 
	{
		First_Checkbox.click();
		Second_Checkbox.click();
		WaitStatement.iSleep(2);
		act.moveToElement(Choose_Action).build().perform();
		WaitStatement.iSleep(2);
		Choose_Action.click();
		WaitStatement.iSleep(2);
		String assign_folio_investor=assign_folio.getText();
		System.out.println(assign_folio_investor);
		String demerge_clientt=demerge_client.getText();
		System.out.println(demerge_clientt);
		String updateNumber=update_number.getText();
		System.out.println(updateNumber);
		for(WebElement e: Folio_lookup_All_Actions)
		{
			String values=e.getText();
			System.out.println(values);
			if(values.contains("Update UCC")) {
			soft.assertTrue(values.equals(assign_folio_investor)|values.equals(demerge_clientt)|values.equals(updateNumber), "Folio Actions Names are not accurate");
			soft.assertAll();
			}else if(values.contains("Update IIN")) {
				soft.assertTrue(values.equals(assign_folio_investor)|values.equals(demerge_clientt)|values.equals(updateNumber), "Folio Actions Names are not accurate");
				soft.assertAll();	
			}else if(values.contains("Update CAN"))
			{
				soft.assertTrue(values.equals(assign_folio_investor)|values.equals(demerge_clientt)|values.equals(updateNumber), "Folio Actions Names are not accurate");
				soft.assertAll();
			}
		}
	}

	
	
	
	public void Verify_AssignFolioToExistingInvestor_Functionality() 
	{
		String Searchnamelike="nikhil";
		String Investor_Mappedto=First_ApplicantMapped.getText();
		String applicant_folio=First_Investor.getText();
		System.out.println(Investor_Mappedto);
		System.out.println(applicant_folio);
		First_Checkbox.click();
//		Proceed_Button.click();
	    Choose_Action.click();
        WaitStatement.iSleep(3);		
		act.moveToElement(assign_folio).build().perform();
        WaitStatement.iSleep(1);		
		assign_folio.click();
		
		Choose_Search.sendKeys(Searchnamelike);
        WaitStatement.iSleep(3);		
		String mapped=Choose_FirstRecordAfterSearch.getText();
		String str[]=mapped.split("/");
		System.out.println(str[0]);
		Choose_FirstRecordAfterSearch.click();
        WaitStatement.iSleep(5);		
		Assign_Folio_Button.click();
        WaitStatement.iSleep(5);		
		String mapped_after=First_ApplicantMapped.getText();
		System.out.println(mapped_after);
		System.out.println(str[0]);
		soft.assertTrue(mapped_after.equals(str[0]), "Assign Folios To Existing Investor is not working");
		soft.assertAll();
		
		gm.Refresh_Page();
		ApplyFilter();
        WaitStatement.iSleep(5);		
		First_Checkbox.click();
//		Proceed_Button.click();
	    Choose_Action.click();
        WaitStatement.iSleep(3);		
		act.moveToElement(assign_folio).build().perform();
        WaitStatement.iSleep(1);		
		assign_folio.click();
		
		Choose_Search.sendKeys(applicant_folio);
        WaitStatement.iSleep(7);
//        Choose_Investor_Search.sendKeys(Keys.ENTER);
        gm.scroll_page_verticall(25);
//        gm.scroll_page_verticall(-250);
        
		String mapped2=Choose_FirstRecordAfterSearch.getText();
		String str2[]=mapped2.split("/");
		System.out.println(str2[0]);
		Choose_FirstRecordAfterSearch.click();
        WaitStatement.iSleep(5);		
		Assign_Folio_Button.click();
        WaitStatement.iSleep(5);		
		String mapped_before=First_Investor.getText();
		System.out.println(mapped_before);
		System.out.println(str[0]);
		soft.assertTrue(mapped_before.equals(str[0]), "Assign Folios To Existing Investor is not working");
		soft.assertAll();
		
		
	}
	
	@FindBy(xpath="//div[@class='popUpArea posRelative']/div/h2")private WebElement AreYouSure_Text;
	@FindBy(xpath="//div[@class='buttons inside']/a[1]")private WebElement AreYouSure_Yes_Button;
	@FindBy(xpath="//div[@class='buttons inside']/a[2]")private WebElement AreYouSure_No_Button;
	@FindBy(xpath="//div[@class='tableListing tableContainers tableShadowOff']/h2")private WebElement No_New_Investor_Created;
	@FindBy(xpath="//div[@class='popUpArea autoPopupArea undefined']/div/div/h2")private WebElement New_Investor_Created;
	@FindBy(xpath="//div[@class='buttons inside']/a")private WebElement Ok_Button_No_New_Investor_Created;

	
	
	public void Verify_AssignfoliotonewInvestor_Functionality() 
	{
		
		
		First_Checkbox.click();

		Choose_Action.click();
        WaitStatement.iSleep(2);		
		act.moveToElement(demerge_client).build().perform();
        WaitStatement.iSleep(2);		
		demerge_client.click();
        WaitStatement.iSleep(2);		
		create_assign.click();
        WaitStatement.iSleep(2);		
		
		//No Scenario
		String areyousure=AreYouSure_Text.getText();
		soft.assertEquals(areyousure, "Are you sure ?");
		AreYouSure_No_Button.click();
		soft.assertTrue(Second_Investor.isDisplayed(),"Are You Sure No button is not working");
		
		//Yes Scenario
	    String investor=First_ApplicantMapped.getText();
		String pan=ApplicantPAN.getText();
		
		WaitStatement.iSleep(2);
		First_Checkbox.click();
		Choose_Action.click();
		WaitStatement.iSleep(2);
		
		act.moveToElement(demerge_client).build().perform();
		WaitStatement.iSleep(2);
		demerge_client.click();
		create_assign.click();
		WaitStatement.iSleep(2);
		AreYouSure_Yes_Button.click();
		
		String NoNew=No_New_Investor_Created.getText();
		if(investor.length()>=1 && pan.length()>=1)
		{
			soft.assertEquals(NoNew, "No New Investor created");
			WaitStatement.iSleep(2);
			Ok_Button_No_New_Investor_Created.click();
			WaitStatement.iSleep(2);
			soft.assertAll();
			
			return;
		}
		else
		{
			soft.assertFalse(NoNew.equals("No New Investor created"), "Asign Folio to new Investor is not working as expected");
			soft.assertAll();
			Ok_Button_No_New_Investor_Created.click();
			WaitStatement.iSleep(2);
			return;
		}
		}

	@FindBy(xpath="//div[@class='tableListing tableContainers tableShadowOff']/div/div/div/table/thead/tr/th/span")private List<WebElement> New_InvestorCreated_Table_Header_Values;
	@FindBy(xpath="//div[@class='tableListing tableContainers tableShadowOff']/div/div/div/table/tbody/tr/td[1]/span")private WebElement New_InvestorCreated_Investor_Name;
	public void Verify_AssignfoliotonewInvestor_WithoutPAN_Functionality() 
	{
		PAN.click();
		WaitStatement.iSleep(2);				

		PAN.click();
		WaitStatement.iSleep(2);				

		
		String investor_name_before=First_ApplicantMapped.getText();
		
       WaitStatement.iSleep(2);		
       First_Checkbox.click();
       WaitStatement.iSleep(2);		
		Choose_Action.click();
	       WaitStatement.iSleep(2);		
		act.moveToElement(demerge_client).build().perform();
	       WaitStatement.iSleep(2);		
		demerge_client.click();
		create_assign.click();
		AreYouSure_Yes_Button.click();
	       WaitStatement.iSleep(2);		

	    String InvestorCreated_Text=New_Investor_Created.getText();
		String joinInvestorCreated_Text=InvestorCreated_Text.replaceAll("\\W", "");
		
		soft.assertTrue(joinInvestorCreated_Text.equalsIgnoreCase("1NewInvestorcreatedwithfollowingdetails"), "New invetor created text mismatch as:: "+InvestorCreated_Text+"");
		
		
		for(int n=0;n<New_InvestorCreated_Table_Header_Values.size();n++)
		{
			WebElement e=New_InvestorCreated_Table_Header_Values.get(n);
			String header=e.getText();
				soft.assertTrue(header.equals("Name") | header.equals("PAN"), "New investor created Table Header Mis Match");
			soft.assertAll();		
		}
		
		String investor_name_after=New_InvestorCreated_Investor_Name.getText();
		System.out.println(investor_name_after+ "   >>>>>");
		soft.assertTrue(investor_name_after.equals(investor_name_before), "Name mismatch after asign folios to new investor");
		Ok_Button_No_New_Investor_Created.click();
		soft.assertAll();
	}

	
	public void Verify_mapped_number() throws SQLException, Exception
	{
		
		for(WebElement e:FolioLookup_Table_Header)
		{
			String p=e.getText();
			System.out.println(p);
			if(p.contains("UCC Number"))
			{
		String uccno = db.Database_SelectQuery_Execution("select uccNumber from folios where uccNumber is not NULL limit 1;", "uccNumber");
//		String uccno=gm.Database_SelectQuery_Execution("uccNumber", "folios", "uccNumber", "is not NULL limit 1");
	System.out.println(uccno);
		String name = db.Database_SelectQuery_Execution("select name from clients where appid in (select appid from folios where uccNumber is not NULL ) limit 1;", "name");
	System.out.println(name);
		applicantSearch_mapped.click();
		applicantmapped_place.sendKeys(name);
		applicantmapped_place.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(3);
		gm.scroll_page_verticall(100);
		WaitStatement.iSleep(3);
		iincanucc.click();
		WaitStatement.iSleep(3);

		gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
		WaitStatement.iSleep(3);
		iincanuccSearch.click();
		iincanuccField.sendKeys(uccno);
		iincanuccField.sendKeys(Keys.ENTER);
		WaitStatement.iSleep(3);
		for(int i=0;i<iincanuccRow.size();i++)
		{
			WebElement e1=iincanuccRow.get(i);
			String uccnum=e1.getText();
			WaitStatement.iSleep(3);
			System.out.println(uccnum);
			soft.assertTrue(uccnum.equals(uccno), "UCC Number is available with folio number");
			
			soft.assertAll();
		 }
		}
			else if(p.contains("IIN Number"))
			{
				String iinno = db.Database_SelectQuery_Execution("select customerid from folios where customerid is not NULL limit 1;", "customerid");
				String name = db.Database_SelectQuery_Execution("select name from clients where appid in (select appid from folios where customerid is not NULL) limit 1;", "name");		
				applicantSearch_mapped.click();
				applicantmapped_place.sendKeys(name);
			applicantmapped_place.sendKeys(Keys.ENTER);
			WaitStatement.iSleep(3);
				gm.scroll_page_verticall(100);
				WaitStatement.iSleep(3);
				iincanucc.click();
				WaitStatement.iSleep(3);

				gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
				WaitStatement.iSleep(3);
				iincanuccSearch.click();
				iincanuccField.sendKeys(iinno);
				iincanuccField.sendKeys(Keys.ENTER);
				WaitStatement.iSleep(3);
				for(int i=0;i<iincanuccRow.size();i++)
				{
					WebElement e2=iincanuccRow.get(i);
					String iinnum=e2.getText();
					WaitStatement.iSleep(3);
					System.out.println(iinnum);
					soft.assertTrue(iinnum.equals(iinno), "IIN Number is available with folio number");
					
					soft.assertAll();
			    }
		
			}	else if(p.contains("CAN Number")) 
			{
				String canno = db.Database_SelectQuery_Execution("select canNumber from folios where canNumber is not NULL limit 1;", "canNumber");
				String name = db.Database_SelectQuery_Execution("select name from clients where appid in (select appid from folios where canNumber is not NULL) limit 1;", "name");		
				applicantSearch_mapped.click();
				applicantmapped_place.sendKeys(name);
			applicantmapped_place.sendKeys(Keys.ENTER);
			WaitStatement.iSleep(3);
				gm.scroll_page_verticall(100);
				WaitStatement.iSleep(3);
				iincanucc.click();
				WaitStatement.iSleep(3);

				gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
				WaitStatement.iSleep(3);
//				String first_number=firstiincanucc.getText();
				iincanuccSearch.click();
				iincanuccField.sendKeys(canno);
				iincanuccField.sendKeys(Keys.ENTER);
				WaitStatement.iSleep(3);

				for(int i=0;i<iincanuccRow.size();i++)
				{
					System.out.println("assdfgdsfg");
					WebElement e3=iincanuccRow.get(i);
					String cannum=e3.getText();
					WaitStatement.iSleep(3);
					System.out.println("sasssssssssss");
					System.out.println(cannum);
					soft.assertTrue(cannum.equals(canno), "CAN Number is available with folio number");
					
					soft.assertAll();
				
			}
		}
		
	}
	}

	@FindBy(xpath="//p[@class='filter-icon bygroup genricFilter undefined']/following-sibling::ul/li[2]/ul/li")private List<WebElement> NumberList ;
	@FindBy(xpath="//a[text()='Apply']")private WebElement applybtn ;

	public void Verify_Update_Number() throws Exception
	{
		
		
		First_Checkbox.click();

		Choose_Action.click();
		WaitStatement.iSleep(2);		
		for(WebElement e: Folio_lookup_All_Actions)
		{
			String values=e.getText();
			System.out.println(values);
			if(values.contains("Update UCC")) 
			{
		act.moveToElement(update_number).build().perform();
		WaitStatement.iSleep(2);		
		update_number.click();
		WaitStatement.iSleep(2);		
		String bseprofile =db.Database_SelectQuery_Execution("select count(*) from bseProfile where status='active' and bid='4';","count(*)");
		System.out.println(bseprofile);
		int uccsize=NumberList.size();
		String uccsize2=Integer.toString(uccsize);
		System.out.println("number of ucc:" +uccsize);
		for(int i=0;i<uccsize;i++)
		{
			String optionsValue = NumberList.get(i).getText();
	        System.out.println(optionsValue);	
		}
		soft.assertEquals(bseprofile, uccsize2,"list of ucc no. mismatch");
		
		Choose_Search.click();
		Choose_Search.sendKeys("IW");
//		driver.findElement(searchNo).sendKeys(Keys.ENTER);
		act.moveToElement(Choose_FirstRecordAfterSearch).build().perform();
		Choose_FirstRecordAfterSearch.click();
		WaitStatement.iSleep(2);		
		soft.assertTrue(applybtn.isDisplayed(),"Apply Button didn't displayed");
		applybtn.click();
		WaitStatement.iSleep(2);		
		//No Scenario
			String areyousure=AreYouSure_Text.getText();
			soft.assertEquals(areyousure, "Are you sure ?");
			AreYouSure_No_Button.click();
			WaitStatement.iSleep(2);		
			//Yes Scenario
			First_Checkbox.click();

			Choose_Action.click();
			WaitStatement.iSleep(2);		
			
			act.moveToElement(update_number).build().perform();
			WaitStatement.iSleep(2);		
		    update_number.click();
			WaitStatement.iSleep(2);		
			Choose_Search.click();
			Choose_Search.sendKeys("IW");
//			driver.findElement(searchNo).sendKeys(Keys.ENTER);
			String firstrecordNo = Choose_FirstRecordAfterSearch.getText();
			act.moveToElement(Choose_FirstRecordAfterSearch).build().perform();
			Choose_FirstRecordAfterSearch.click();
			WaitStatement.iSleep(2);		
			soft.assertTrue(applybtn.isDisplayed(),"Apply Button didn't displayed");
			applybtn.click();
			WaitStatement.iSleep(2);		
			String areyousure2=AreYouSure_Text.getText();
			soft.assertEquals(areyousure2, "Are you sure ?");
			AreYouSure_Yes_Button.click();
			WaitStatement.iSleep(2);
			gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
			WaitStatement.iSleep(2);
			String updatedNo = firstiincanucc.getText();
			soft.assertTrue(updatedNo.equals(firstrecordNo), "Update ucc no is not working");
			soft.assertAll();
			}
			else if(values.contains("Update IIN"))
			{
				act.moveToElement(update_number).build().perform();
				WaitStatement.iSleep(2);		
				update_number.click();
				WaitStatement.iSleep(2);		
				
				String nseprofile =db.Database_SelectQuery_Execution("select count(*) from nseProfile where activationStatus='yes' and bid='4';","count(*)");
				System.out.println(nseprofile);
				int iinsize=NumberList.size();
				String iinsize2=Integer.toString(iinsize);
				System.out.println("number of iin:" +iinsize);
				for(int i=0;i<iinsize;i++)
				{
					String optionsValue = NumberList.get(i).getText();
			        System.out.println(optionsValue);	
				}
				soft.assertEquals(nseprofile, iinsize2, "list of iin no. mismatch");
//				soft.assertAll();
				Choose_Search.click();
				Choose_Search.sendKeys("1");
//				driver.findElement(searchNo).sendKeys(Keys.ENTER);
				String firstrecordNo = Choose_FirstRecordAfterSearch.getText();
				act.moveToElement(Choose_FirstRecordAfterSearch).build().perform();
				Choose_FirstRecordAfterSearch.click();
				WaitStatement.iSleep(2);		
				soft.assertTrue(applybtn.isDisplayed(),"Apply Button didn't displayed");
				applybtn.click();
				WaitStatement.iSleep(2);		
				String areyousure2=AreYouSure_Text.getText();
				soft.assertEquals(areyousure2, "Are you sure ?");
				AreYouSure_Yes_Button.click();
				WaitStatement.iSleep(2);
				gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
				WaitStatement.iSleep(2);
				String updatedNo = firstiincanucc.getText();
				soft.assertTrue(updatedNo.equals(firstrecordNo), "Update iin no is not working");
				soft.assertAll();	
			}	
			
			else if(values.contains("Update CAN"))
			{
				act.moveToElement(update_number).build().perform();
				WaitStatement.iSleep(2);		
				update_number.click();
				WaitStatement.iSleep(2);		
				
				String mfuprofile =db.Database_SelectQuery_Execution("select count(*) from mfuProfile where status='AP' and bid='4';","count(*)");
				System.out.println(mfuprofile);
				int cansize=NumberList.size();
				String cansize2=Integer.toString(cansize);
				System.out.println("number of can:" +cansize);
				for(int i=0;i<cansize;i++)
				{
					String optionsValue = NumberList.get(i).getText();
			        System.out.println(optionsValue);	
				}
				soft.assertEquals(mfuprofile, cansize2, "list of iin no. mismatch");
//				soft.assertAll();
				Choose_Search.click();
				Choose_Search.sendKeys("1");
//				driver.findElement(searchNo).sendKeys(Keys.ENTER);
				String firstrecordNo = Choose_FirstRecordAfterSearch.getText();
				act.moveToElement(Choose_FirstRecordAfterSearch).build().perform();
				Choose_FirstRecordAfterSearch.click();
				WaitStatement.iSleep(2);		
				soft.assertTrue(applybtn.isDisplayed(),"Apply Button didn't displayed");
				applybtn.click();
				WaitStatement.iSleep(2);		
				String areyousure2=AreYouSure_Text.getText();
				soft.assertEquals(areyousure2, "Are you sure ?");
				AreYouSure_Yes_Button.click();
				WaitStatement.iSleep(2);	
				gm.Horizontal_scroll(Horizontalscroller, iincanuccSearch);
				WaitStatement.iSleep(2);	

				String updatedNo = firstiincanucc.getText();
				soft.assertTrue(updatedNo.equals(firstrecordNo), "Update can no is not working");
				soft.assertAll();
				
				
			}
		}			
	}
	
	
	
	
}



